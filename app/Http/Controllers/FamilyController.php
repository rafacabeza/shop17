<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Family;

class FamilyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        //autorización a través del user model
        $family = new Family();
        $user = $request->user();
        if (! $user->can('view', $family)) {
            return redirect('/home');
        }
        //$families = Family::all();
        $families = Family::paginate(4);
        if ($request->ajax()) {
            return $families;
        } else {
            return view('family.index', ['families' => $families]);
        }
    }

    public function ajax()
    {
        return view('family.ajax');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('create', Family::class);
        return view('family.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
        'code' => 'required|max:4|unique:families',
        'name' => 'required|max:40',
        ]);

        $family = new Family($request->all());
        $family->save();
        if ($request->ajax()) {
            return response()->json(['done']);
        } else {
            return redirect('/families');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, Request $request)
    {
//        $family = Family::find(1);
        $family = Family::findOrFail($id);
        $this->authorize('view', $family);
        if ($request->ajax()) {
            return $family;
        } else {
            return view('family.show', ['family' => $family]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $family = Family::findOrFail($id);
        $this->authorize('update', $family);
        return view('family.edit', ['family' => $family]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //unique:table,column,except,idColumn

        $this->validate($request, [
        'code' => 'required|max:4|unique:families,id,' . $id,
        'name' => 'required|max:40',
        ]);

        $family = Family::findOrFail($id);
        $this->authorize('update', $family);
        $family->code = $request->code;
        $family->name = $request->name;
        $family->save();
        if ($request->ajax()) {
            return response()->json(['done']);
        } else {
            return redirect('/families');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request)
    {
        Family::find($id)->delete();
        $this->authorize('delete', $family);
        //Family::destroy($id);
        if ($request->ajax()) {
            return response()->json(['done']);
        } else {
            return redirect('/families');
        }
    }

    public function showProducts($id, Request $request)
    {
        $family = Family::findOrFail($id);
        $products = $family->products();
        $products = $family->products()->orderBy('code')->paginate();

        if ($request->ajax()) {
            return $products;
        } else {
            return view('family.products', [
                'family' => $family,
                'products' => $products
                ]);
        }
    }
}
