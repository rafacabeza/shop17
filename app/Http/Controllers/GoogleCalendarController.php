<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \Carbon\Carbon;
use \Google_Client;
use \Google_Service_Calendar;
use \Google_Service_Calendar_Event;

class GoogleCalendarController extends Controller
{
    protected $client;  //conexión API google
    protected $service; //conexión a un servicio google, en nuestro caso calendar

    const CALENDAR_ID =
        'pgv5au0ogrmrrn57kto40t8hf4@group.calendar.google.com';

    public function __construct()
    {
        define('APPLICATION_NAME', 'shop17');
        define('CREDENTIALS_PATH', '../storage/app/google/client_secret.json');
        define('SCOPES', implode(' ', array(
            Google_Service_Calendar::CALENDAR
            )));

        $this->client = new Google_Client();

        $this->client->setApplicationName(APPLICATION_NAME);
        $this->client->setScopes(SCOPES);
        $this->client->setAuthConfig(CREDENTIALS_PATH);
        // $this->client->setAccessType('offline');

        $this->service = new Google_Service_Calendar($this->client);
        $this->calendar = $this->service->calendars->get($this::CALENDAR_ID);

        setlocale(LC_TIME, 'es_ES.UTF-8');

    }

    public function index()
    {
        $events = $this->service->events->listEvents($this::CALENDAR_ID);
        return view('event.index', ['events' => $events, 'calendar' => $this->calendar]);
    }

    public function show($id)
    {
        $event = $this->service->events->get($this::CALENDAR_ID, $id);
        return view('event.show', ['event' => $event]);
    }


    public function create()
    {
        return view('event.create');
    }


    public function store(Request $request)
    {
        //tiempos leidos en texto
        $date = $request->start;
        $startTime = $request->startTime;
        $endTime = $request->endTime;
        //tiempos pasados a Carbon
        $dateStart = Carbon::createFromFormat('Y-m-d H:i', $date . $startTime, 'Europe/Madrid');
        $dateEnd = Carbon::createFromFormat('Y-m-d H:i', $date . $endTime, 'Europe/Madrid');

        //reconvertidos a texto formateado correctamente
        $dateStart = $dateStart->format('Y-m-d\TH:i:sP');
        $dateEnd = $dateEnd->format('Y-m-d\TH:i:sP');


        // echo $dateStart . '<br>';
        // echo $dateEnd . '<br>';
        // dd($dateStart);

        $event = new Google_Service_Calendar_Event(array(
          'summary' => $request->summary,
          'location' => 'Aula A25',
          'description' => $request->description,
          'start' => array(
            'dateTime' => $dateStart,
            'timeZone' => 'Europe/Madrid',
          ),
          'end' => array(
            'dateTime' => $dateEnd,
            'timeZone' => 'Europe/Madrid',
          ),
        ));

        $calendarId = $this::CALENDAR_ID;
        $event = $this->service->events->insert($this::CALENDAR_ID, $event);

        return redirect('/events');
    }

    public function destroy($id)
    {
        $this->service->events->delete($this::CALENDAR_ID, $id);
        return redirect('/events');
    }



    //...................................................................
    //ejemplos de uso de carbon .........................................
    //...................................................................
    public function carbon()
    {
        $carbon = new Carbon;
        $now = $carbon->now(); //según uso UTC
        $now = $carbon->now('UTC'); //idem
        $now = $carbon->now('Europe/Madrid'); //uso de Madrid
        $now = $carbon->now('Asia/Tokyo'); //uso de Tokio

        echo '<h3>Formato general según http://php.net/manual/es/function.date.php </h3>';
        echo $now->format('l jS \of F Y h:i:s A');

        echo '<h3>Formato Localizado según http://php.net/manual/es/function.strftime.php</h3>';
        echo $now->formatLocalized('%A %d de %B %Y %A');

        echo '<hr>';

        $now = Carbon::create(2017, 8, 1, 12, 0, 0);
        $now = $carbon->create(2017, 8, 1);
        $now->setTimeZone('Asia/Tokyo');

        echo '<h3>Formato general según http://php.net/manual/es/function.date.php </h3>';
        echo $now->format('l jS \of F Y h:i:s A');

        echo '<h3>Formato Localizado según http://php.net/manual/es/function.strftime.php</h3>';
        echo $now->formatLocalized('%A %d de %B %Y %A');


        dd($now);
        // echo $now->toCookieString();       // Thu, 25 Dec 1975 14:15:16 -0500
    }
}
