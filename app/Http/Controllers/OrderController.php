<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Order;
use App\Product;
use Mail;
use Illuminate\Html\HtmlBuilder;
use Illuminate\Support\Facades\Html;
use App\Mail\MailOrder;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {

        $user = $request->user();
        if (is_null($user) || $user->isClient()) {
            $orders = Order::where('user_id', $user->id)->paginate();
        } else {
            $orders = Order::paginate();
        }
        return view('order.index', ['orders' => $orders]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $products = session('products');
        if (is_null($products)) {
            $products = array();
        }
        return view('order.create', ['products' => $products]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $order = new Order;
        $user = $request->user();
        $order->user_id = $user->id;
        $order->save();

        $products = session('products');
        foreach ($products as $product) {
            $order->products()->attach($product['id'], [
                'price' => $product['price'],
                'quantity' => $product['quantity'],
                ]);
        }
        session()->forget('products');


        //crear objeto pdf
        $view =  \View::make('pdf.order', ['order' => $order])->render();
        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadHTML($view);

        //enviar el mailable
        Mail::to($request->user())->send(new MailOrder($order, $pdf));
        return redirect('/orders');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $order = Order::findOrFail($id);
        $this->authorize('view', $order);
        return view('order.show', ['order' => $order]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function addProduct($id, Request $request)
    {
        // \Session::forget('order');
        $product = Product::findOrFail($id);

        $products = session('products');
        $element['id'] = $id;
        $element['quantity'] = 1;
        $element['price'] = $product->price;
        $element['product'] = $product;

        $products[] = $element;
        session(['products' => $products]);
        //\Session::put('products', $products);
        //$request->session()->put('products', $products);

        return redirect('orders/create');
    }


    public function pdfOrder($id)
    {
        $user = \Auth::user();
        $order = Order::findOrFail($id);
        $view =  \View::make('pdf.order', ['order' => $order])->render();
        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadHTML($view);

        //enviar el mailable
        //Mail::to($user)->send(new MailOrder($order, $pdf));


        return $pdf->stream('invoice');
    }

    public function email($id)
    {
        $user = \Auth::user();
        $order = Order::findOrFail($id);
        $view =  \View::make('pdf.order', ['order' => $order])->render();
        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadHTML($view);

        //enviar el mailable
        Mail::to($user)->send(new MailOrder($order, $pdf));


        return back();
    }
}
