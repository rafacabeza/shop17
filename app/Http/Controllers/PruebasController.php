<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class PruebasController extends Controller
{
    public function index($method)
    {
        $this->$method();
    }

    public function products()
    {
        // toda la tabla
        echo DB::table('products')->toSql();
        $result = DB::table('products')->get();
        dd($result);
    }
    public function users()
    {
        // toda la tabla
        echo DB::table('users')->toSql();
        $result = DB::table('users')->get();
        dd($result);
    }
    public function product()
    {
        // toda la tabla
        echo DB::table('products')->toSql();
        $result = DB::table('products')->first();
        dd($result);
    }
    public function user1()
    {
        echo DB::table('users')->select('email', 'name')->toSql();
        $result = DB::table('users')->select('email', 'name')->first();
        dd($result);
    }
    public function families1()
    {
        echo DB::table('families')->select('code')->toSql();
        $result = DB::table('families')->select('code')->get();
        dd($result);
    }
    public function families2()
    {
        echo DB::table('families')->select('code')->where('code', '=', 'PERI')->toSql();
        $result = DB::table('families')->select('code')->where('code', '=', 'PERI')->get();
        dd($result);
    }
    public function families3()
    {
        //Uso de where
        echo DB::table('families')->select('code')->where('code', 'like', 'M%')->toSql();
        $result = DB::table('families')
            ->select('code')
            ->where('code', 'like', 'M%')
            ->where('code', 'like', '%C%')
            ->get();
        dd($result);
    }
    public function families4()
    {
        // toda la tabla
        echo DB::table('families')->select('code')->where('code', 'like', 'M%')->orderBy('code')->toSql();
        $result = DB::table('families')->select('code')->where('code', 'like', 'M%')->orderBy('code')->get();
        dd($result);
    }
    public function families5()
    {
        // toda la tabla
        echo DB::table('families')->select('code')->where('code', 'like', 'M%')->orderBy('code', 'desc')->toSql();
        $result = DB::table('families')->select('code')->where('code', 'like', 'M%')->orderBy('code', 'desc')->get();
        dd($result);
    }
    public function join1()
    {
        // toda la tabla
        echo DB::table('families')->crossJoin('products')->toSql();
        $result = DB::table('families')->crossJoin('products')->get();
        dd($result);
    }
    public function join2()
    {
        // toda la tabla
        echo DB::table('families')->join('products', 'families.id', '=', 'family_id')->toSql();
        $result = DB::table('families')->join('products', 'families.id', '=', 'family_id')->select('families.name', 'products.name as product')->get();
        dd($result);
    }
    public function sql()
    {
        // toda la tabla
        echo DB::raw("SELECT * FROM products WHERE family_id = 3");
        $result = DB::select(DB::raw("SELECT * FROM products WHERE family_id = 3"));
        dd($result);
    }
}
