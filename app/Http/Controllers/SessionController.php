<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SessionController extends Controller
{
    public function index($method = 'todo', $key = 'clave', $value = 'a')
    {
        $this->$method($key, $value);
    }

    public function poner($key, $value)
    {
        session([$key => $value]);

        $this->ver($key);
        dd(session()->all());
    }

    public function ver($key)
    {
        echo '<hr>ver <br>';
        $elemento = session($key);
        // $elemento = session()->get($key);
        echo $key . ': ' .$elemento . '<br>';


    }

    public function todo()
    {
        dd(session()->all());
    }
}
