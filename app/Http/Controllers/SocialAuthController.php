<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Socialite;
use App\User;
use \Auth;

class SocialAuthController extends Controller
{
    public function redirect()
    {
        return Socialite::driver('google')->redirect();
    }

    public function callback()
    {
        try {
            $user = Socialite::driver('google')->user();
        } catch (\Exception $e) {
            return redirect('/login')->with('status', 'Something went wrong or You have rejected the app!');
        }

        $authUser = $this->findOrCreateUser($user);

        Auth::login($authUser);

        return redirect('/home');
    }

    public function findOrCreateUser($user)
    {
        $authUser = User::where('email', $user->email)->first();

        if ($authUser) {
            return $authUser;
        }

        return User::create([
            'name' => $user->name,
            'email' => $user->email,
            'surname' => $user->user['name']['familyName'],
            'password' => 'empty',
            // 'google_id' => $user->id,
            // 'avatar' => $user->avatar
        ]);
        // recordatorio: create equivale a new() + asignar campos + save()
    }
}
