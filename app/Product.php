<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = ['code', 'name', 'price', 'family_id'];
    public function family()
    {
        return $this->belongsTo('App\Family');
    }

    public function orders()
    {
        return $this->belongsToMany('App\Order')->withPivot('quantity', 'price');
    }

}
