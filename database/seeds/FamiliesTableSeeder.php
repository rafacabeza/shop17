<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class FamiliesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('families')->insert([
            'code' => 'MICR',
            'name' => 'Microprocesadores',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        
        DB::table('families')->insert([
            'code' => 'PERI',
            'name' => 'Perifericos',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        
        DB::table('families')->insert([
            'code' => 'STOR',
            'name' => 'Almacenamiento',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        
        DB::table('families')->insert([
            'code' => 'MEDI',
            'name' => 'Multimedia',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        
    }
}
