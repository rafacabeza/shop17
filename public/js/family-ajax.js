$(document).ready(function () {

    /*Load Data List*/
    var loadData = function () {
        url = '/family';
        $.get(url, "", function (data) {
            // alert(data);
            $("#tbodyMain").empty();

            for (var i = 0; i < data.length; i++) {
                var newRow = '<tr id="row' + data[i].id + '" idRow=' + data[i].id + ' >';
                newRow = newRow + '<td > ' + data[i].id + '</td>';
                newRow = newRow + '<td > ' + data[i].code + '</td>';
                newRow = newRow + '<td > ' + data[i].name + '</td>';
                newRow = newRow + '<td data-id="'+data[i].id+'">';
                newRow = newRow + '<button data-toggle="modal" data-target="#edit-item" class="btn btn-primary edit-item">Edit</button> '; 
                newRow = newRow + '<button class="btn btn-danger remove-item">Delete</button>';
                newRow = newRow + '</td>';
                newRow = newRow + '<td id="delete' + data[i].id + '" class="link"> <a>Borrar</a> </td></tr>';
                $("#tbodyMain").append(newRow);                            
            }
        }, 'json');
    };






    /* Remove Item */

    $("body").on("click",".remove-item",function(){
        var id = $(this).parent("td").data('id');
        var c_obj = $(this).parents("tr");
        $.ajax({
            dataType: 'json',
            type:'delete',
            url: url + '/' + id,
        }).done(function(data){
            c_obj.fadeOut();
            // loadData();
        });

    });


    loadData();

    /*Prepara cabeceras para envio del token csrf*/
    $.ajaxSetup({
        headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
    });


})
