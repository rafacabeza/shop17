@extends('layouts.mail')
@section('content')

<h1>Informe de pedido</h1>
<h2>Cliente:  {{ $order->user->name }}</h2>
<h3>Fecha: {{ $order->updated_at }}</h3>

    <table>
    <tr>
        <th>Producto</th>
        <th>Precio</th>
        <th>Cantidad</th>
        <th>Total</th>
    </tr>

    @foreach ($order->products as $product)
        <tr>
            <td>{{ $product->name }}
            <td>{{ $product->pivot->price }}
            <td>{{ $product->pivot->quantity }}
            <td>{{ $product->pivot->price * $product->pivot->quantity }}
        </tr>
    @endforeach
    </table>

@endsection('content')

