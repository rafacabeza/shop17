@extends('layouts.app')

@section('content')
    <h1>Lista de eventos</h1>
    <h3>Calendario {{ $calendar->summary }}</h3>
    <p>{{ $calendar->description }}</p>

    <a href="/events/create">Nuevo</a>


    <table class="table">
        <thead>
            <tr>
                <th>Id</th>
                <th>Desde </th>
                <th>Hasta </th>
                <th>Evento</th>
                <th>Descripción </th>
            </tr>
        </thead>
        <tbody>
        @foreach ($events->getItems() as $event)
            <tr>
                <td>  {{ $event->id }} </td>
                <td>  {{ $event->start->date . $event->start->dateTime  }} </td>
                <td>  {{ $event->end->date . $event->start->dateTime  }} </td>
                <td>  {{ $event->summary }} </td>
                <td>  {{ $event->description }} </td>
                <td>  
                    <form method="post" action="/events/{{ $event->id }}">
                        <input type="hidden" name="_method" value="DELETE">
                        {{ csrf_field() }}

                        @can('delete', $event)
                        <input type="submit" value="Borrar">
                        @endcan

                        @can('update', $event)
                        <a href="/events/{{ $event->id }}/edit">Editar</a>
                        @endcan

                        <a href="/events/{{ $event->id }}"> Ver </a>






                        <form method="post" action="/events/{{ $event->id }}">
                            <input type="hidden" name="_method" value="DELETE">
                            {{ csrf_field() }}

                            <input type="submit" value="Borrar">

                        </form>





                    </form>


                </td>
            </tr>
        @endforeach
        </tbody>
    </table>


@endsection('content')