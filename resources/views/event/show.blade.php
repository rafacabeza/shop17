@extends('layouts.app')
@section('content')

        <h1>Detalle de evento</h1>
        <p>Id: {{ $event->id }}</p>
        <p>Código: {{ $event->summary }}</p>
        <p>Nombre: {{ $event->description }}</p>
        @if ($event->start->date)
            <p>Desde : {{ $event->start->date }}</p>
            <p>Hasta : {{ $event->end->date }}</p>
            
            <p>Hasta : {{ \Carbon\Carbon::createFromFormat('Y-m-d', $event->start->date)->format('d-m-Y') }}</p>
        @else 
            <p>Desde : {{ $event->start->dateTime }}</p>
            <p>Hasta : {{ $event->end->dateTime }}</p>
        @endif

@endsection('content')