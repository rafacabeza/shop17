@extends('layouts.app')
@section('content')

    <h1>Editar datos de familia</h1>
    <div class="form">
    <form  action="/families" method="post">
    {{ csrf_field() }}


    <div class="form-group">
        <label>Codigo: </label>
        <input type="text" name="code" value="{{ old('code') }}">
        {{ $errors->first('code') }}
    </div>
    <div class="form-group">
        <label>Nombre: </label>
        <input type="text" name="name" value="{{ old('name') }}">
        {{ $errors->first('name') }}
    </div>
    <input type="submit" value="Guardar">
    </form>
    </div>

@endsection('content')