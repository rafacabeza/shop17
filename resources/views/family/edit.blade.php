@extends('layouts.app')
@section('content')

    <h1>Editar datos de familia</h1>
    <div class="form">
    <form  action="/families/{{ $family->id }}" method="post">
    {{ csrf_field() }}
    <input type="hidden" name="_method" value="PUT">

    <div class="form-group">
        <label>Id: </label>
        <input type="text" name="id" value="{{ $family->id }}" readonly="readonly">
    </div>
    <div class="form-group">
        <label>Codigo: </label>
        <input type="text" name="code" value="{{ old('code',  $family->code) }}">
        {{ $errors->first('code') }}
    </div>
    <div class="form-group">
        <label>Nombre: </label>
        <input type="text" name="name" value="{{ old('name',  $family->name) }}">
        {{ $errors->first('name') }}
    </div>
    <input type="submit" value="Guardar">
    </form>
    </div>

@endsection('content')