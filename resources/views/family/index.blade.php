@extends('layouts.app')

@section('content')
    <h1>Lista de familias</h1>
    @can('create', 'App\Family')
        <a href="/families/create">Nuevo</a>
    @else
        No puedes dar altas!!
    @endcan


    <table class="table">
        <thead>
            <tr>
                <th>Id</th>
                <th>Código</th>
                <th>Nombre</th>
                <th>Acciones</th>
            </tr>
        </thead>
        <tbody>
        @foreach ($families as $family)
            <tr>
                <td>  {{ $family->id }} </td>
                <td>  {{ $family->code }} </td>
                <td>  {{ $family->name }} </td>
                <td>  
                    <form method="post" action="/families/{{ $family->id }}">
                        <input type="hidden" name="_method" value="DELETE">
                        {{ csrf_field() }}

                        @can('delete', $family)
                        <input type="submit" value="Borrar">
                        @endcan

                        @can('update', $family)
                        <a href="/families/{{ $family->id }}/edit">Editar</a>
                        @endcan

                        @can('view', $family)
                        <a href="/families/{{ $family->id }}"> Ver </a>
                        @endcan
                    </form>


                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

    {{ $families->render() }}

@endsection('content')