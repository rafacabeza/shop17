@extends('layouts.app')
@section('content')

        <h1>Detalle de familia</h1>
        <p>Id: {{ $family->id }}</p>
        <p>Código: {{ $family->code }}</p>
        <p>Nombre: {{ $family->name }}</p>

        <h1>Lista de productos</h1>

    <table class="table">
        <thead>
            <tr>
                <th>Id</th>
                <th>Código</th>
                <th>Nombre</th>
                <th>Precio</th>
                <th>Familia</th>
                <th>Acciones</th>
            </tr>
        </thead>
        <tbody>
        @foreach ($products as $product)
            <tr>
                <td>  {{ $product->id }} </td>
                <td>  {{ $product->code }} </td>
                <td>  {{ $product->name }} </td>
                <td>  {{ number_format($product->price, 2, "'", ".") }} €   </td>
                <td>  {{ $product->family->name }} </td>
                <td>  
                    <form method="post" action="/products/{{ $product->id }}">
                        <input type="hidden" name="_method" value="DELETE">
                        {{ csrf_field() }}
                        <input type="submit" value="Borrar">
                        <a href="/products/{{ $product->id }}/edit">Editar</a>
                        <a href="/products/{{ $product->id }}"> Ver </a>
                    </form>


                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

    {{ $products->render() }}


@endsection('content')