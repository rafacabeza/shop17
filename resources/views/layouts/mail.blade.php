<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{ config('app.name', 'Laravel') }} email</title>
    <!-- Styles -->
    <link href="/css/app.css" rel="stylesheet">
    <style>
    <?php include('../resources/assets/css/pdf.css') ?>
    </style
</head>


<body>

        <div class="container">
            @yield('content')            
        </div>

        
</body>
</html>