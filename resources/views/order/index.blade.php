@extends('layouts.app')

@section('content')
    <h1>Lista de Pedidos</h1>
    @can('create', 'App\order')
        No puedes dar altas!!
    @else
    @endcan


    <table class="table table-striped table-bordered table-hover">
        <thead>
            <tr>
                <th>Id</th>
                <th>Fecha</th>
                <th>Importe</th>
                <th>Operación</th>
            </tr>
        </thead>
        <tbody>
        @foreach ($orders as $order)
            <tr>
                <td>  {{ $order->id }} </td>
                <td>  {{ $order->created_at->format('d-m-Y') }} </td>
                <td>  {{ $order->cost() }}€ </td>
                <td>   
                    <form method="post" action="/orders/{{ $order->id }}">
                        <input type="hidden" name="_method" value="DELETE">
                        {{ csrf_field() }}


                        <a href="/orders/{{ $order->id }}"> Ver </a>
                        <!-- @can('view', $order) -->
                        <!-- @endcan -->
                    </form>


                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

    {{ $orders->render() }}

@endsection('content')