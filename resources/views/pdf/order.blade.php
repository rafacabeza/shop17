<!DOCTYPE html>
<html>
<head>
    <title></title>
    <link rel="stylesheet" href="/home/usuario/proyectos/shop17/resources/assets/css/pdf.css" media="all" />



</head>
<body>
<h1>Informe de pedido</h1>
<h2>Cliente:  {{ $order->user->name }}</h2>
<h3>Fecha: {{ $order->updated_at }}</h3>

    <table>
    <tr>
        <th>Producto</th>
        <th>Precio</th>
        <th>Cantidad</th>
        <th>Total</th>
    </tr>

    @foreach ($order->products as $product)
        <tr>
            <td>{{ $product->name }}
            <td>{{ $product->pivot->price }}
            <td>{{ $product->pivot->quantity }}
            <td>{{ $product->pivot->price * $product->pivot->quantity }}
        </tr>
    @endforeach
    </table>
</body>
</html>