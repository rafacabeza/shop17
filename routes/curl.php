<?php

Route::get('curl', function () {
    
    $handler = curl_init("http://www.google.es");
    $response = curl_exec($handler);
    curl_close($handler);
    echo $response;
});

Route::get('curl2', function () {
    $client = new \GuzzleHttp\Client();
    $res = $client->request('GET', 'http://httpbin.org');
    echo $res->getStatusCode() . '<br>';
    // 200
    echo $res->getHeaderLine('content-type') . '<br>';
    // 'application/json; charset=utf8'
    echo $res->getBody();
    // '{"id": 1420053, "name": "guzzle", ...}'
});

Route::get('curl3', function () {
    $client = new \GuzzleHttp\Client();
    // Send an asynchronous request.
    $request = new \GuzzleHttp\Psr7\Request('GET', 'http://httpbin.org');
    $promise = $client->sendAsync($request)->then(function ($response) {
        echo 'I completed! ' . $response->getBody();
    });
    echo 'Vamos a esperar.... <br>';
    $promise->wait();
});


Route::get('curl4', function () {
    $curl = curl_init();


    $url = 'https://opendata.aemet.es/opendata/api/maestro/municipios';
    $url = 'https://opendata.aemet.es/opendata/api/prediccion/especifica/municipio/diaria/50031';
    $url = 'https://opendata.aemet.es/opendata/sh/943bd117';

    curl_setopt_array($curl, array(
      CURLOPT_URL => "$url?api_key=eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJyYWZhY2FiZXphQGdtYWlsLmNvbSIsImp0aSI6IjFkMTkzM2FkLWEwZDktNDk4Ny04Y2YwLTc4NmM0NGRlYTY3MSIsImV4cCI6MTQ5NjUwMjg0MCwiaXNzIjoiQUVNRVQiLCJpYXQiOjE0ODg3MjY4NDAsInVzZXJJZCI6IjFkMTkzM2FkLWEwZDktNDk4Ny04Y2YwLTc4NmM0NGRlYTY3MSIsInJvbGUiOiIifQ.UC4Kr8AIH4joQ7G0sjovAvJ8kbrEPxwwg60PkkY8BVg",
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 30,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "GET",
      CURLOPT_HTTPHEADER => array(
        "cache-control: no-cache"
      ),
    ));

    $response = curl_exec($curl);
    $err = curl_error($curl);

    curl_close($curl);

    if ($err) {
        echo "cURL Error #:" . $err;
    } else {
        echo '<pre>' . $response . '</pre>';

        $response = iconv('UTF-8', 'UTF-8//IGNORE', utf8_encode($response));
        
        dd(json_decode($response, true));
        // echo json_last_error();  // returns 5 ?

    }
});



Route::get('curl5', function () {
    $url = 'https://opendata.aemet.es/opendata/api/maestro/municipios';
    $url = 'https://opendata.aemet.es/opendata/api/prediccion/especifica/municipio/diaria/50031';
    $url = 'https://opendata.aemet.es/opendata/sh/943bd117';

    $apiKey = 'eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJyYWZhY2FiZXphQGdtYWlsLmNvbSIsImp0aSI6IjFkMTkzM2FkLWEwZDktNDk4Ny04Y2YwLTc4NmM0NGRlYTY3MSIsImV4cCI6MTQ5NjUwMjg0MCwiaXNzIjoiQUVNRVQiLCJpYXQiOjE0ODg3MjY4NDAsInVzZXJJZCI6IjFkMTkzM2FkLWEwZDktNDk4Ny04Y2YwLTc4NmM0NGRlYTY3MSIsInJvbGUiOiIifQ.UC4Kr8AIH4joQ7G0sjovAvJ8kbrEPxwwg60PkkY8BVg';

    $client = new \GuzzleHttp\Client();
    $res = $client->request('GET', $url, ['headers' => ['api_key' => $apiKey]]);
    echo $res->getStatusCode() . '<br>';
    // 200
    echo $res->getHeaderLine('content-type') . '<br>';

    // echo $res->getBody();

    $data = iconv('UTF-8', 'UTF-8//IGNORE', utf8_encode($res->getBody()));
        
        dd(json_decode($data, true));
});
