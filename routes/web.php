<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index');
Route::get('/casa', function () {
    return 'Página home';
})->middleware('admin');


Route::get('families/ajax', 'FamilyController@ajax');
Route::resource('families', 'FamilyController');
Route::get('families/{id}/products', 'FamilyController@showProducts');

Route::resource('products', 'ProductController');



//rutas de pruebas
Route::get('fluent/{method}', 'PruebasController@index');
// Route::get('sesion', 'SessionController@index');
Route::get('sesion/{method?}/{key?}/{value?}', 'SessionController@index');


Route::resource('orders', 'OrderController');
Route::get('/orders/products/{id}', 'OrderController@addProduct');


//rutas para emails
Route::get('mail', function () {
    $user = Auth::user();
    echo 'enviamos mensaje.... <br>';
    Mail::send('emails.prueba', ['user' => $user], function ($message) use ($user) {
        $message->from('no-reply@shop17.com', 'Shop17');
        $message->to($user->email, $user->name)->subject('Your Reminder!');
    });
    return 'mensaje enviado';
});
    // Mail::send('vista', 'datos', function () {})
        // $message->from($address, $name = null);
        // $message->sender($address, $name = null);
        // $message->to($address, $name = null);
        // $message->cc($address, $name = null);
        // $message->bcc($address, $name = null);
        // $message->replyTo($address, $name = null);
        // $message->subject($subject);
        // $message->priority($level);
        // $message->attach($pathToFile, array $options = []);

Route::get('mail2', 'MailController@index');
Route::get('orders/{id}/pdf', 'OrderController@pdfOrder');
Route::get('orders/{id}/email', 'OrderController@email');

Route::get('/redirect/google', 'SocialAuthController@redirect');
Route::get('/callback/google', 'SocialAuthController@callback');

Route::resource('events', 'GoogleCalendarController');
Route::get('/carbon', 'GoogleCalendarController@carbon');

require('curl.php');
